-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Bulan Mei 2021 pada 08.53
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id` int(4) NOT NULL,
  `id_card_number` varchar(16) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id`, `id_card_number`, `name`, `place_of_birth`, `date_of_birth`, `gender`, `address`) VALUES
(1, '11323', 'Jimmy', 'Malang', '2020-05-08', 'L', 'Jl Magelang 01'),
(2, '67565', 'Fallen', 'Jakarta', '2020-05-08', 'P', 'Jl Tembus 002'),
(3, '20030', 'Jaka', 'Surabaya', '2020-05-08', 'L', 'Gang Gabus jalan utama');

-- --------------------------------------------------------

--
-- Struktur dari tabel `price_room`
--

CREATE TABLE `price_room` (
  `id` int(4) NOT NULL,
  `room_type_id` int(4) NOT NULL,
  `price` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `updated_by` varchar(15) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `room_category`
--

CREATE TABLE `room_category` (
  `id` int(4) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_by` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `room_category`
--

INSERT INTO `room_category` (`id`, `name`, `description`, `active`, `created_by`, `created_at`) VALUES
(1, 'Large', 'size >= 7x7', 1, 'admin', '2020-05-08 05:41:15'),
(2, 'Medium', 'size >= 5.5x5.5', 1, 'admin', '2020-05-08 05:41:17'),
(3, 'Small', 'size >= 4.5x4.5', 1, 'admin', '2020-05-08 05:41:20'),
(4, 'Family', 'size >= 5x7', 0, 'admin', '2020-05-08 05:42:31'),
(5, 'Exclusive', '-', 0, 'admin', '2020-05-08 05:42:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room_type`
--

CREATE TABLE `room_type` (
  `id` int(4) NOT NULL,
  `room_category_id` int(4) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_room`
--

CREATE TABLE `stock_room` (
  `id` int(4) NOT NULL,
  `room_type_id` int(4) NOT NULL,
  `stock_total` int(4) DEFAULT NULL,
  `stock_available` int(4) DEFAULT NULL,
  `stock_ordered` int(4) DEFAULT NULL,
  `created_by` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(15) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` int(4) NOT NULL,
  `number` varchar(255) DEFAULT NULL,
  `customer_id` int(4) DEFAULT NULL,
  `created_by` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_history_detail`
--

CREATE TABLE `transaction_history_detail` (
  `id` int(4) NOT NULL,
  `transaction_history_id` int(4) DEFAULT NULL,
  `room_type_id` int(4) DEFAULT NULL,
  `room_ordered` int(4) DEFAULT NULL,
  `quantity` int(4) DEFAULT NULL,
  `price` int(4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `price_room`
--
ALTER TABLE `price_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `price_room_fk` (`room_type_id`);

--
-- Indeks untuk tabel `room_category`
--
ALTER TABLE `room_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_type_fk` (`room_category_id`);

--
-- Indeks untuk tabel `stock_room`
--
ALTER TABLE `stock_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_room_fk` (`room_type_id`);

--
-- Indeks untuk tabel `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaction_history_fk` (`customer_id`);

--
-- Indeks untuk tabel `transaction_history_detail`
--
ALTER TABLE `transaction_history_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaction_history_detail_fk` (`transaction_history_id`),
  ADD KEY `transaction_history_detail_fk_1` (`room_type_id`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `price_room`
--
ALTER TABLE `price_room`
  ADD CONSTRAINT `price_room_fk` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Ketidakleluasaan untuk tabel `room_type`
--
ALTER TABLE `room_type`
  ADD CONSTRAINT `room_type_fk` FOREIGN KEY (`room_category_id`) REFERENCES `room_category` (`id`);

--
-- Ketidakleluasaan untuk tabel `stock_room`
--
ALTER TABLE `stock_room`
  ADD CONSTRAINT `stock_room_fk` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);

--
-- Ketidakleluasaan untuk tabel `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `transaction_history_fk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Ketidakleluasaan untuk tabel `transaction_history_detail`
--
ALTER TABLE `transaction_history_detail`
  ADD CONSTRAINT `transaction_history_detail_fk` FOREIGN KEY (`transaction_history_id`) REFERENCES `transaction_history` (`id`),
  ADD CONSTRAINT `transaction_history_detail_fk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
